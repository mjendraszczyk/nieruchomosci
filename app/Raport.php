<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Raport extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $primaryKey = 'id_raport';
    public $incrementing = true;
    protected $table = 'raport';
    protected $fillable = [
        'nr_oferty', 'id_serwisu', 'url', 'cena', 'data_dodania','telefon','zdjecie','powierzchnia','powierzchnia_dzialki','rok_budowy','cena_m2','wlasciciel','id_user'
    ];

}
