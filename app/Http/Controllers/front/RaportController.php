<?php

namespace App\Http\Controllers\front;

use Goutte;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RaportController extends Controller
{
    public $data; // Dane z crawla
    public $oferty = array(); // tablica ofert
    public $oferty_nazwa = array(); 
    public $oferty_link = array(); 
    public $oferty_numer = array(); 
    public $oferty_cena = array(); 
    public $props; // własciwosci do tablicy
    public $att; // Atrybut html
    
    public $qty;
    //
    //https://www.otodom.pl/frontera/api/item/owner/phone/174-160-20-23-219-227-167-167-65-195-38-13-98-120-205-94-124-88-241-5-42-105-3-32-252-35-122-224-88-233-121-63-9-167-36-117
    public function store(Request $request)  {
            // dd($request);

            echo "<h3>";
            echo "Raport sprzedazy nieruchomosci dla podanych kryteriów";
    
            echo "</h3>";
            echo "<a href='/'>Powrót</a>";
            //https://www.otodom.pl/sprzedaz/dom/pyrzyce/?search%5Bregion_id%5D=16&search%5Bsubregion_id%5D=362&search%5Bcity_id%5D=2111
//https://www.otodom.pl/sprzedaz/dom/pyrzyce/?search%5Bdist%5D=50
        $url = "https://www.otodom.pl/sprzedaz/";
        if($request->input('rodzaj')) {
            $url .= $request->input('rodzaj').'/';
        }
        if($request->input('miejscowosc')) {
            $url .= str_slug($request->input('miejscowosc')).'/';
        }
        if ($request->input('cena_od')) {
            $url .= "?search%5Bfilter_float_price%3Afrom%5D=".str_slug($request->input('cena_od')).'/';
        }
        if ($request->input('cena_do')) {
            if(empty($request->input('cena_od'))) {
                $url .= "?";
            } else {
                $url .= "&";
            }
            $url .= "search%5Bfilter_float_price%3Ato%5D=".str_slug($request->input('cena_do')).'/';
        }
        if($request->input('odleglosc')) {
            $url .= '&search%5Bdist%5D='.$request->input('odleglosc').'';
            $url .= "&nrAdsPerPage=72";
        }

        echo $url;
      
    // $ilosc_ofert = 0;
    // $crawler = Goutte::request('GET', $url);
    // $crawler->filter('.offers-index > strong')->each(function ($node) {
    //     $ilosc_ofert = trim($node->text());
    //     return $ilosc_ofert;
    // });

       // echo "tst".$this->getQtyOffers($url,".offers-index > strong");
  //  

//   data-tracking-id="47081000"
      //echo "URL:".$url;
        $this->oferty_nazwa =  $this->getOffers($url, [".offer-item-title"], [null], ["nazwa"]);
        $this->oferty_link = $this->getOffers($url, [".offer-item-header > h3 > a"], ["href"], ["url"]);
        $this->oferty_numer = $this->getOffers($url, [".offer-item"], ["data-tracking-id"], ["oferty_numer"]);
        $this->oferty_cena = $this->getOffers($url, [".offer-item-price"], [null], ["cena"]);
        //$this->getQtyOffers($url, ".offer-item-header > h3 > a", "href", "url");
       foreach($this->oferty_nazwa as $key => $o) {
           $this->oferty[$key]['nazwa'] = $o;
       }

       foreach($this->oferty_link as $key => $o) {
           $this->oferty[$key]['url'] = $o;
       }

       foreach($this->oferty_numer as $key => $o) {
           $this->oferty[$key]['oferty_numer'] = $o;
       }
       foreach($this->oferty_cena as $key => $o) {
           $this->oferty[$key]['cena'] = $o;
       }
    //    echo "N".print_r($this->getOffers($url,".offer-item-header > h3 > a"));
    echo "<table>";
    echo "<tr>";
    echo "<td>";
    echo "ID";
    echo "</td>";
    echo "<td>";
    echo "Obraz";
    echo "</td>";
    echo "<td>";
    echo "Nazwa";
    echo "</td>";
    echo "<td>";
    echo "Link";
    echo "</td>";
    echo "<td>";
    echo "Cena";
    echo "</td>";
    echo "</tr>";
        foreach($this->oferty as $oferta) {
            echo "<tr>";
            
            echo "<td>";
            echo $oferta["oferty_numer"];
            echo "</td>";

            echo "<td>";
            
            echo "<img src='".$this->getOffers($oferta["url"], [".css-pzaa1o"], ["src"], ["image"])[0]."' style='width:128px;'/>";
            echo "</td>";

            echo "<td>";
            echo "<h3>";
            echo $oferta["nazwa"];
            echo "</h3>";
            
            echo "</td>";
            echo "<td><a target='_blank' href='".$oferta["url"]."'>";
            echo $oferta["url"];
            echo "</a></td>";
            echo "<td>";
            echo $oferta["cena"];
            echo "</td>";
            
             echo "</tr>";
//         echo "<br/>";
// // echo "<br/>Link: ".$this->getQtyOffers($url, ".offer-item-header > h3 > a", "href");
       
       }
        
        // foreach()
    // $sortedPrices = array();
    //     foreach($this->getCurrentPrices() as $key => $prices) {
    //         if(($key %4 )==0) { 
    //             $indeks = $prices;
    //         } else {
    //             $sortedPrices[$indeks][] = $prices;
    //         }

    // $crawler->filter('.offer-item-title')->each(function ($node) {

    //   dump($node->text());
    // });

    }
    public function getQty($url, $selector) {
         $crawler = Goutte::request('GET', $url);
         $crawler->filter($selector)->each(function ($node) {
             $this->qty = trim($node->text());
         });
         return $this->qty;
    }
    //  public function getOffers($url, $selector) {
    //         $crawler = Goutte::request('GET', $url);
                
    //             $crawler->filter($selector)->each(function ($node) {
    //                     $url = trim($node->attr('href'));
    //                     $nazwa = trim($node->attr('title'));
    //                 return compact($nazwa, $url);
    //         });
    //     }
    
    public function getOffers($url, $selector, $type, $properties) {
        $this->data = [];
            foreach ($properties as  $key => $property) {
                $this->props = $property;
                $crawler = Goutte::request('GET', $url);
                $this->att = $type[$key];
                $crawler->filter($selector[$key])->each(function ($node) {
                    if ($this->att != null) {
                        $this->data[] = trim($node->attr($this->att));
                    } else {
                        $this->data[] = trim($node->text());
                    }
                });
            }
    return $this->data;
    }
}
