<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRaportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('raport', function (Blueprint $table) {
            $table->bigIncrements('id_raport');
            $table->string('nr_oferty');
            $table->integer('id_serwisu');
            $table->string('url');
            $table->float('cena');
            $table->date('data_dodania');
            $table->string('telefon');
            $table->string('zdjecie');
            $table->float('powierzchnia');
            $table->float('powierzchnia_dzialki')->nullable();
            $table->integer('rok_budowy')->nullable();
            $table->float('cena_m2')->nullable();
            $table->string('wlasciciel');
            $table->integer('id_user');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('raport');
    }
}
