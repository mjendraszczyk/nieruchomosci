@extends('layouts.nieruchomosci')
@section('page')profil @endsection
@section('sidebar')
<div class="sidebar">
    <div class="logo_content"><a href="http://127.0.0.1:8000"><img
                src="https://image.flaticon.com/icons/svg/553/553335.svg" class="logo"></a>
    </div>
    <span class="logo_slogan">Expert nieruchomości</span>
    <div class="card">
        <div class="card-body">
            <ul>
                <li>
                    <a href="#">Panel</a>
                </li>
                <li>
                    <a href="#">Baza ofert</a>
                </li>
                <li>
                    <a href="#">Płatnosci</a>
                </li>
                <li>
                    <a href="#">Ustawienia</a>
                </li>
            </ul>
        </div>
    </div>
</div>
@endsection
@section('content')
<div class="container">
    {{-- <div class="row justify-content-center"> --}}
    <div class="row stats">
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <span class="material-icons">
                        attach_money
                    </span>
                    Saldo
                    0.00 PLN
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <span class="material-icons">
                        home
                    </span>
                    Liczba ofert
                    100
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <span class="material-icons">
                        list
                    </span>
                    Liczba rabortów
                    2
                </div>
            </div>
        </div>
    </div>
    <div class="row">

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Panel</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif

                    <div class="row">
                        <div class="col-md-6">
                            <a href="#" class="btn btn-primary raport-btn">Przeglądaj oferty</a>
                            <a href="#" class="btn btn-success raport-btn">Zarządzaj płatnościami</a><a href="#"
                                class="btn btn-secondary raport-btn">Zarządzaj raportami</a></div>

                        <div class="col-md-6">
                        </div>

                    </div>
                </div>
            </div>
        </div>
        {{-- </div> --}}
    </div>
</div>
@endsection