@extends('layouts.nieruchomosci')
@section('page')home @endsection
@section('content')
<div class="row" class="padding25">
    <div class="headline">
        <img src="https://image.flaticon.com/icons/svg/553/553335.svg" class="logo white-text">
        <b class="white-text">Twój expert w branży nieruchomości</b>
    </div>
    <form method="post" action="{{route('raport_store')}}" class="padding15 white">
        @csrf
        <div class="content-text">Generuj <b class="focus-text">automatyczne raporty o nieruchomościach</b>,
            które Cię interesują.
            Stwórz listę ofert
            nieruchomości według
            kryteriów</label>
            <div class="row">
                <div class="col-md-3">
                    <label>Rodzaj nieruchomości</label>
                    <select class="form-control" name="rodzaj">
                        <option value="dom">Dom</option>
                        <option value="mieszkanie">Mieszkanie</option>
                        <option value="dzialka">Działka</option>
                        <option value="lokal">Lokal</option>
                        <option value="garaz">Garaz</option>
                        <option value="haleimagazyny">Hale i magazyny</option>
                    </select>
                </div>
                <div class="col-md-3">
                    <label>Cena</label>
                    <div class="row">
                        <div class="col-md-6">
                            <input type="number" min="0" class="form-control" name="cena_od">
                        </div>
                        <div class="col-md-6">
                            <input type="number" min="0" class="form-control" name="cena_do">
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <label>Powierzchnia</label>
                    <div class="row">
                        <div class="col-md-6">
                            <input type="number" min="0" class="form-control" name="powierzchnia_od">
                        </div>
                        <div class="col-md-6">
                            <input type="number" min="0" class="form-control" name="powierzchnia_do">
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <label>Miejscowosc</label>
                    <div class="row col-md-8 inline"><select name="miejscowosc" class="form-control">
                            <option value="Pyrzyce">Pyrzyce</option>
                            <option value="Stargard">Stargard</option>
                            <option value="Szczecin">Szczecin</option>
                            <option value="Gorzów Wielkopolski">Gorzów Wielkopolski</option>
                            <option value="Poznań">Poznań</option>
                            <option value="Gdańsk">Gdańsk</option>
                            <option value="Wrocław">Wrocław</option>
                            <option value="Warszawa">Warszawa</option>
                            <option value="Katowice">Katowice</option>
                            <option value="Kraków">Kraków</option>
                            <option value="Rzeszów">Rzeszów</option>
                        </select>
                    </div>
                    <div class="row col-md-4 inline"><select name="odleglosc" class="form-control">
                            <option value="0">+0 km</option>
                            <option value="5">+5 km</option>
                            <option value="10">+10 km</option>
                            <option value="25">+25 km</option>
                            <option value="50">+50 km</option>
                            <option value="75">+75 km</option>
                            <option value="100">+100 km</option>
                            <option value="150">+150 km</option>
                            <option value="200">+200 km</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-12 text-center">
                    <div class="row">
                        <div class="col-md-8">
                            <button type="submit" class="btn btn-success raport-btn">
                                <span class="material-icons">
                                    save_alt
                                </span>Wyślij
                                raport</button>
                        </div>
                        <div class="col-md-4">
                            <button type="button" class="btn btn-info raport-btn">
                                <span class="material-icons">
                                    videocam
                                </span>Zobacz demo
                            </button>
                        </div>
                    </div>
                </div>
            </div>
    </form>
</div>

</div>
@endsection