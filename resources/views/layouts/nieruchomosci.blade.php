<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Expert nieruchomości</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="{{asset('css/app.css')}}" rel="stylesheet">
    <link href="{{asset('css/nieruchomosci.css')}}" rel="stylesheet">
</head>

<body class="flex-center @yield('page')">

    {{-- Top --}}
    <section class="top">
        {{-- <div class="flex-center position-ref full-height"> --}}
        <div class="top-right links">
            <div class="logo_content"><a href="{{route("homepage")}}"><img
                        src="https://image.flaticon.com/icons/svg/553/553335.svg" class="logo"></a>
            </div>
            <span class="logo_slogan">Expert nieruchomości</span>
            <div class="login links">
                @if (Route::has('login'))
                @auth
                <span class="material-icons">
                    attach_money
                </span> <a href="#">0.00 PLN</a>

                <span class="material-icons">
                    person_outline
                </span> <a href=" {{ route('profil') }}">Panel</a>

                <span class="material-icons">
                    lock
                </span> <a href="#">Wyloguj</a>
                @else
                <span class="material-icons">
                    lock_open
                </span><a href="{{ route('login') }}">Logowanie</a>

                @if (Route::has('register'))
                <span class="material-icons">
                    person_outline
                </span> <a href="{{ route('register') }}">Rejestracja</a>
                @endif
                @endauth
                @endif
            </div>
        </div>
    </section>
    {{-- Content --}}
    @yield('sidebar')
    <div class="content">
        @yield('content')
    </div>
    {{-- Footer --}}
    <section class="footer">
        <div class="container">
            <div class="row links">
                <div class="col-lg-6">
                    <a href="#">O serwisie</a>
                    <a href="#">Cennik</a>
                    <a href="#">Regulamin</a>
                    <a href="#">Polityka prywatności</a>
                    <a href="#">Kontakt</a>
                </div>
                <div class="col-lg-6 text-right">
                    © 2020 developed by <img src="https://dystrybutory-prime.pl/themes/prime/assets/img/mages_brand.png"
                        class="mages_brand">
                </div>
            </div>
        </div>
    </section>
</body>

</html>