<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('homepage');


Route::post('/raport/wynik', 'front\RaportController@store')->name('raport_store');
// Route::get('/kontakt', 'frontend\CmsController@kontakt')->name('kontakt');
// Route::get('/o-nas', 'frontend\CmsController@onas')->name('onas');

Auth::routes();

Route::get('/profil', 'front\HomeController@index')->name('profil');
